import time
import datetime
import pandas as pd
from selenium import webdriver
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
''' 
SE LLENAN LOS FORMULARIOS WEB PARA TODOS LOS GRUPOS DEL ARCHIVO MAESTRO,
PASO A PASO:
 1) EL ARCHIVO MAESTRO SOLO DEBE CONTENER FILAS DE UN SOLO TUTOR
 2) HAY QUE BORRAR LAS FILAS CON TRIPULANTES SIN GRUPO ASIGNADO
 3) ORDENAR LAS FILAS SEGUN LA COLUMNA EQUIPO DE PROYECTO
 4) BORRAR LAS FILAS DE EQUIPOS YA CARACTERIZADOS (EVITAR REDUNDANCIA)
 5) EXPORTAR A CSV
 6) CAMBIAR LA VARIABLE tutor, sprint A CARACTERIZAR, desarrollo = "web" o "movil"
 Y LA BANDERA envio_manual (EL SUBMIT NO LO HACE EL SCRIPT)
 7) EJECUTAR Y DISFRUTAR
 
 
PD: PARA MOVIL SOLO FUNCIONA HASTA EL SPRINT 3
DESDE LA LINEA 141 SE PUEDE EDITAR LAS RESPUESTAS DE LA DESCRIPCION DEL SEGUIMIENTO
POR CADA FORMULARIO LLENADO SE REGISTRA EL EQUIPO EN LOS LOGS
CON EL FIN DE QUE LOS EQUIPOS EN LOS LOGS NO SE TENGAN EN CUENTA PARA
EL REGISTRO QUE YA SE REALIZO
'''

sprint=1
tutor=""
desarrollo="web"
envio_manual=True

def set_up():
    from selenium.webdriver.firefox.options import Options as FirefoxOptions

    options = FirefoxOptions()
    options.add_argument("--headless")
    return webdriver.Firefox(executable_path='./Drivers/geckodriver.exe')

def enviar_formulario_sprint(browser: WebDriver, formulario = {}, tutor="", sprint=1, dev="web", manual=True):
    '''
    Envia los formularios extraidos con la funcion csv2formularios
    Args:
        browser: WebDriver - objeto
        formulario: dict - diccionario con los datos suficientes para llenar un formulario
        tutor: str - nombres y apellidos completos del tutor
        sprint: int - numero de sprint a caracterizar
        dev: str - desarrollo web: "web", desarrollo movil: "cualquier_string"
    Returns:
        
    Raises:
        NoSuchElementException - si el elemento a buscar no se encuentra en el HTML
    Usage:
        enviar_formulario_sprint(set_up(), {
            'GRUPO':'B10',
            'EQUIPO': 'GRUPO 1',
            'CODIGOS': [111,222,333],
            'PARTICIPACION': [True,False,False],
            }, "foo", 3)
    '''
    
    if dev == "web":
        links_formularios=[
            'https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUOUtIMjdQVzNUMjhGWTJBNUJWRDg2U01RWC4u',
            'https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUMUw5N0pOSTEzUTZVUVkzQlpSTDNXQVdDUS4u',
            'https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUMFoxRTg4R1NBTEhZUTZJMkI3MjBJTk5OSy4u',
            'https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdURTdMNFRaV1ZURlhNNTBWNUcwSUZUWkozNS4u',
            'https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUQk9SUU5XUDNQVEkyQlBWSkNIN0lKVUdQQS4u',
        ]
    else:
        links_formularios=[
            'https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUMkRTSDQwUTM5QzJJWEFMUlFUWTdKV0E3MC4u',
            'https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUMENDSzBHUEhSUFlXSFJDMUc0WUc5REo4US4u',
            'https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUNDM0WENCNUoyN0xUUFdQQUMyV1EwU0pSMi4u',
            'https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUNUdSNzFVSlNJWVVXREoxNTRCWDFET0hURC4u',
            'https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUQ1JIR0xBMzM2WjM0TVlMSDNOVzhRSTk4Mi4u',
        ]
    
    # Cargar la primera página del formulario
    browser.get(links_formularios[sprint-1])
    try:
        primer_check = WebDriverWait(browser, 5).until(EC.presence_of_element_located((By.NAME, 'r4a43c9c93d3a48bc91e428c2416979b1')))
        primer_check.click()
    except TimeoutException:
        print('error cargando la pagina')
    
    
    fecha = browser.find_element_by_xpath('//input[@aria-labelledby="QuestionId_r76f9ecfc332340789b7e26dff2507025 QuestionInfo_r76f9ecfc332340789b7e26dff2507025"]')
    fecha.send_keys(datetime.date.today().strftime('%d/%m/%Y'))

    browser.find_element_by_xpath('//input[@value="'+tutor+'"]').click()

    grupo = browser.find_element_by_xpath('//input[@aria-labelledby="QuestionId_rabaa60b77bd3465e94108df18b34cd0d QuestionInfo_rabaa60b77bd3465e94108df18b34cd0d"]')
    grupo.send_keys(formulario['GRUPO'])
    equipo=browser.find_element_by_xpath('//input[@value="'+formulario['EQUIPO']+'"]')
    equipo.click()

    # Pasar a la página siguiente
    browser.find_element_by_xpath('//button[@aria-label="Siguiente"]').click()
    WebDriverWait(browser, 5)
    
    radio_btn_codigos=["QuestionId_r75d7d6a6070745b2a2e524ffc7d69402 QuestionInfo_r75d7d6a6070745b2a2e524ffc7d69402",
    "QuestionId_r2d4d71480ead4321b0118871784b5c59 QuestionInfo_r2d4d71480ead4321b0118871784b5c59",
    "QuestionId_r84cdb564073d4ba7a71e2283231abf39 QuestionInfo_r84cdb564073d4ba7a71e2283231abf39",
    "QuestionId_rbff44bce3c89432d80a92f3900557768 QuestionInfo_rbff44bce3c89432d80a92f3900557768",
    "QuestionId_r4c58321cf81840b5b663e401d54b6297 QuestionInfo_r4c58321cf81840b5b663e401d54b6297",
    "QuestionId_r3218dec9102f453a89561a956b47966c QuestionInfo_r3218dec9102f453a89561a956b47966c"]
    
    for id, codigo in enumerate(formulario['CODIGOS']):
        browser.find_element_by_xpath('//input[@aria-labelledby="'+radio_btn_codigos[id]+'"]').send_keys(codigo)
        participacion = formulario['PARTICIPACION'][id]
        if participacion:
            browser.find_element_by_xpath('//input[@aria-label="Integrante '+str(id+1)+', Evidenciado"]').click()
        else:
            browser.find_element_by_xpath('//input[@aria-label="Integrante '+str(id+1)+', No evidenciado"]').click()
    
    # Pasar a la página siguiente
    browser.find_element_by_xpath('//button[@aria-label="Siguiente"]').click()
    WebDriverWait(browser, 5)
    
    if dev=="web":
        btnChecks=["r0fec66a27fa4449783e83ace3a63c1fe", #pregunta 13
        "rcfc30609fb254ba9a0b1d7cd3b6b37fa",            #pregunta 14
        "r0293979a639741e8a45d3e0c8ab7f032",            #pregunta 15
        "r640d731a6ed1474b8808e3a343c9acb6",            #pregunta 16
        "r81e10f7f23eb4fcfa6020df136849982"             #pregunta 17
        ]
        if sprint == 3 or sprint == 4 or sprint == 5:
            btnChecks.append("r52913b843f614e11ad00b0e302d313da")
        if sprint == 5:
            btnChecks.append("r2df0b3dfb5b248518d5e62b8c9c3ff86")
    else:
        btnChecks=["r0fec66a27fa4449783e83ace3a63c1fe", #pregunta 13
        "rcfc30609fb254ba9a0b1d7cd3b6b37fa",            #pregunta 14
        "r6e13e58cf7934c8e8be15deee9c8175d",            #pregunta 15
        "r640d731a6ed1474b8808e3a343c9acb6",            #pregunta 16
        "r81e10f7f23eb4fcfa6020df136849982"             #pregunta 17
        ]
        
        if sprint == 2 or sprint == 3:
            btnChecks.append("r0293979a639741e8a45d3e0c8ab7f032")


    #pregunta 18 (text_area) 
    descripcion_seguimiento=browser.find_element_by_xpath('//textarea[@aria-labelledby="QuestionId_r5fe32032b1e742f088f0744f76f86434 QuestionInfo_r5fe32032b1e742f088f0744f76f86434"]')
    
    #Se llenan los campos según la participacion del equipo
    if True in formulario['PARTICIPACION']:
        if False in formulario['PARTICIPACION']:
            descripcion_seguimiento.send_keys('Algunos de los integrantes no participan')
        else:
            descripcion_seguimiento.send_keys('Todos los integrantes estan participando')
        for btn_txt in btnChecks:
            
            if dev=='web':
                browser.find_element_by_xpath('//input[@name="'+btn_txt+'"][@value="Evidenciado"]').click()
            else:
                browser.find_element_by_xpath('//input[@name="'+btn_txt+'"][@aria-posinset="4"]').click()
    else:
        descripcion_seguimiento.send_keys('No hay participación en el grupo')
        for btn_txt in btnChecks:
            if dev=='web':
                browser.find_element_by_xpath('//input[@name="'+btn_txt+'"][@value="No evidenciado"]').click()
            else:
                browser.find_element_by_xpath('//input[@name="'+btn_txt+'"][@aria-posinset="1"]').click()
            

    if manual:
        input('INGRESE CUALQUIER TECLA PARA LLENAR EL SIGUIENTE FORMULARIO: ')
    else:
        # Enviar formulario
        browser.find_element_by_xpath('//button[@title="Enviar"]/div[1]').click()
        time.sleep(2)
        

    


def csv2formularios(sprint=1, path_to_csv_data='./datos.csv', sep=';'):
    
    '''
    extrae los datos del archivo csv para convertilos en un arreglo de diccionarios
    Args:
        sprint: int - número del sprint a adelantar
        path_to_csv_data: string - ruta relativa o absoluta del archivo .csv
        (solo es convertir el excel archivo maestro en csv)
        sep: str - separador de los campos en el csv
    Returns:
        formularios: Lista de diccionarios para cada formulario
    Raises:
        NoFieldElementException - hay que quitar la primera fila del archivo maestro 
        (DATOS DEL TRIPULANTE, DESARROLLO DE APLICACIONES WEB Y MOVIL, ETC)

    Usage:
        csv2formularios(1, './datos.csv', sep=';')
    '''
    
    def dict_furmulario(grupo, equipo, codigos = [], participacion = []):
        return {
            'GRUPO':grupo,
            'EQUIPO': equipo,
            'CODIGOS': codigos,
            'PARTICIPACION': participacion,
            }
    
    # Arreglando los titulos del csv
    headers_df = pd.read_csv(path_to_csv_data, sep, nrows=1, header = None)
    headers = headers_df.values.tolist()[0]
    fixed_headers = [x.replace(' ','_') for x in headers]
    
    columnas_a_usar = ['CODIGO_UIS','ENTREGA_SPRINT_1','ENTREGA_SPRINT_2','ENTREGA_SPRINT_3',
                        'EQUIPO_DE_PROYECTO','GRUPO']
    
    data = pd.read_csv(path_to_csv_data, sep, header=0, names=fixed_headers, 
                       usecols=columnas_a_usar)
    
    first_equipo=True
    formularios=[]
    codigo_trip=[]
    sprint_enviado_por_trip=[]
    for i, row in data.iterrows():
        if first_equipo:
            equipo_current = row.EQUIPO_DE_PROYECTO.strip()
            grupo_current = row.GRUPO.strip()
        if(row.EQUIPO_DE_PROYECTO.strip() == equipo_current and row.GRUPO.strip() == grupo_current):
            codigo_trip.append(row.CODIGO_UIS)
            if 'Enviado para calificar' in row.get(columnas_a_usar[sprint]):
                sprint_enviado_por_trip.append(True)
            else:
                sprint_enviado_por_trip.append(False)
            first_equipo=False
        else:
            # Limpiar campos: Grupo 01 -> Grupo 1
            equipo = equipo_current.split('-')[1]
            if equipo[-2] == '0':
                equipo = equipo[:-2] + equipo[-1:]
            
            formularios.append(dict_furmulario(grupo_current, equipo.upper(), codigo_trip, sprint_enviado_por_trip))
            
            equipo_current = row.EQUIPO_DE_PROYECTO
            grupo_current = row.GRUPO
            codigo_trip=[]
            sprint_enviado_por_trip=[]

            codigo_trip.append(row.CODIGO_UIS)
            if 'Enviado' in row.get(columnas_a_usar[sprint]):
                sprint_enviado_por_trip.append(True)
                
            else:
                sprint_enviado_por_trip.append(False)
            
    return formularios




if __name__=='__main__':
    driver = set_up() 
    with open('logs.txt', 'a+') as logs_w, open('logs.txt') as logs_r:
        
        lines = [line.split(',') for line in logs_r.readlines()]
        formularios = csv2formularios(sprint)
        
        for i, formulario in enumerate(formularios):
            fill_form = True
            for row in lines:
                if str(sprint) in row[0] and formulario['EQUIPO'] in row[1] and formulario['GRUPO'] in row[2]:
                    fill_form = False
            if fill_form:
                    print(f"Llenando el formulario para {formulario['EQUIPO']}, {formulario['GRUPO']} \n")
                    enviar_formulario_sprint(driver, formulario, tutor, sprint, desarrollo, envio_manual)
                    logs_w.write('INFO: Se lleno el formulario sprint {},{},{}\n'.format(sprint,formulario['EQUIPO'],formulario['GRUPO']))
        driver.close()